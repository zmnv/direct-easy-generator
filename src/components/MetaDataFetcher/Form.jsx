import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { Button, Label, Input } from '~/theme/components';

class Form extends PureComponent {
    handleSubmit = event => {
        event.preventDefault();
        const form = event.target;
        const data = new FormData(form);
        const formDataObject = {};

        // eslint-disable-next-line no-restricted-syntax
        for (const name of data.keys()) {
            const input = form.elements[name];
            Object.assign(formDataObject, { [input.name]: input.value });
        }

        if (typeof this.props.onSubmit === 'function') {
            this.props.onSubmit(formDataObject);
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} autoComplete="off">
                <Label htmlFor="sourceUrl">
                    <h2 className="text-center mar-b-48">Что будем рекламировать?</h2>
                    <Input
                        type="text"
                        name="sourceUrl"
                        id="sourceUrl"
                        placeholder="Вставьте ссылку"
                        autoFocus
                        required
                    />
                </Label>

                <div className="mar-t-16 text-center">
                    <Button type="submit" color="mono" bold>
                        {this.props.isLoading ? 'Загрузка...' : 'Создать баннер'}
                    </Button>
                </div>
            </form>
        );
    }
}

Form.propTypes = {
    onSubmit: PropTypes.func,
    isLoading: PropTypes.bool,
};

Form.defaultProps = {
    onSubmit: null,
    isLoading: false,
};

export default Form;
