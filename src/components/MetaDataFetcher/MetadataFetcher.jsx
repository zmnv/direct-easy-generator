import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Snackbar } from '@material/react-snackbar';

import { connect } from 'react-redux';

import { fetchMetadata } from '~/apps/dashboard/store';

import Form from './Form';

const mapStateToProps = state => ({
    isLoading: state.dashboard && state.dashboard.isLoading,
    errorMessage: state.dashboard && state.dashboard.error,
});

const mapDispatchToProps = dispatch => ({
    fetchMetadata: (url, callback) => dispatch(fetchMetadata(url, callback)),
});

class MetadataFetcher extends PureComponent {
    snackbarRef = React.createRef();

    onFormSubmit = values => {
        if (values && values.sourceUrl) {
            this.props.fetchMetadata(values.sourceUrl, this.props.onMetadataReady);
        }
    }

    renderSnackbar() {
        const { errorMessage } = this.props;

        if (!errorMessage) {
            return null;
        }

        return (
            <Snackbar
                ref={this.snackbarRef}
                timeoutMs={10000}
                message={errorMessage}
                actionText="закрыть"
            />
        );
    }

    render() {
        return (
            <Fragment>
                <Form
                    onSubmit={this.onFormSubmit}
                    isLoading={this.props.isLoading}
                />
                {this.renderSnackbar()}
            </Fragment>
        );
    }
}

MetadataFetcher.propTypes = {
    onMetadataReady: PropTypes.func,
};

MetadataFetcher.defaultProps = {
    onMetadataReady: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(MetadataFetcher);
