import styled from 'styled-components';

const Warning = styled.div`
    font-size: 13px;
    color: #c52f00;
`;

export default Warning;
