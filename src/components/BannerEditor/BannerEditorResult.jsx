import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { getBannerMetadata } from '~/apps/dashboard/store/dashboardSelectors';

import { getHtmlTemplate } from '~/helpers/getHtmlTemplate';
import { TextareaClipboard } from '~/theme/components/Textarea';

import { Label } from '~/theme/components';

function createBannerMarkup(state) {
    return {__html: getHtmlTemplate(state)};
}

const mapStateToProps = state => ({
    data: getBannerMetadata(state),
});

class BannerEditorResult extends PureComponent {
    renderCard(params = {}) {
        const bannerData = this.props.data;
        const { onlyHtml } = params;

        if (onlyHtml) {
            return getHtmlTemplate(bannerData);
        }

        return (
            // eslint-disable-next-line react/no-danger
            <div dangerouslySetInnerHTML={createBannerMarkup(bannerData)} />
        );
    }

    render() {
        return (
            <div>
                <div className="mar-b-16">
                    <Label className="mar-b-16">Результат</Label>
                    {this.renderCard()}
                </div>
                <div className="row mar-b-24">
                    <div className="col-12">
                        <TextareaClipboard actionButtonLabel="Скопировать код баннера" value={this.renderCard({ onlyHtml: true })} />
                    </div>
                </div>
            </div>
        );
    }
}

BannerEditorResult.propTypes = {
    data: PropTypes.object,
};

BannerEditorResult.defaultProps = {
    data: {},
};

export default connect(mapStateToProps)(BannerEditorResult);
