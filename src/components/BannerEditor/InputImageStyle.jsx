import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

import { Label, Checkbox } from '~/theme/components';

class InputImageStyle extends PureComponent {
    setContain = event => {
        const isContain = event.target.checked;
        this.props.onChange({ isContain });
    }

    setBlured = event => {
        const isBlured = event.target.checked;
        this.props.onChange({ isBlured });
    }

    render() {
        const { values: { isContain, isBlured } } = this.props;

        return (
            <Fragment>
                <Label>Настройки изображения</Label>

                <div style={{display: 'flex'}}>
                    <div className="mar-r-16">
                        <Checkbox
                            id="thumbContain"
                            label="Показать целиком"
                            checked={isContain}
                            onChange={this.setContain}
                        />
                    </div>

                    <Checkbox
                        id="bluredImage"
                        label="Размытый фон"
                        checked={isBlured}
                        onChange={this.setBlured}
                    />
                </div>
            </Fragment>
        );
    }
}

InputImageStyle.propTypes = {
    values: PropTypes.object,
    onChange: PropTypes.func,
};

InputImageStyle.defaultProps = {
    values: {},
    onChange: () => {},
};

export default InputImageStyle;
