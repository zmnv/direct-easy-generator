/* eslint-disable jsx-a11y/no-autofocus */
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import {
    getBannerMetadata,
    dashboardUpdateMetadata,
    dashboardCleanMetadata,
} from '~/apps/dashboard/store';

import { AutofocusDisabler } from '~/theme/components';

import InputContent from './InputContent';
import InputHref from './InputHref';
import InputImageStyle from './InputImageStyle';

const mapStateToProps = state => ({
    bannerData: getBannerMetadata(state),
});

const mapDispatchToProps = dispatch => ({
    updateMetadata: data => dispatch(dashboardUpdateMetadata(data)),
    cleanMetadata: () => dispatch(dashboardCleanMetadata()),
});

class BannerEditorForm extends PureComponent {
    handleUpdateMetadata = value => {
        this.props.updateMetadata(value);
    }

    handleCleanMetadata = () => {
        this.props.cleanMetadata();
    }

    render() {
        const {
            isAdvanced,
            bannerData: {
                title, description, image, href, isContain, isBlured,
            },
        } = this.props;

        return (
            <Fragment>
                <AutofocusDisabler />
                <div className="row">
                    <div className="col-12 mar-b-16">
                        <InputHref
                            value={href}
                            onChange={this.handleUpdateMetadata}
                        />
                        <InputContent
                            visible={isAdvanced}
                            values={{title, description, image}}
                            onChange={this.handleUpdateMetadata}
                            onClean={this.handleCleanMetadata}
                        />
                    </div>

                    <div className="col-12">
                        <InputImageStyle
                            values={{isContain, isBlured}}
                            onChange={this.handleUpdateMetadata}
                        />
                    </div>
                </div>
            </Fragment>
        );
    }
}

BannerEditorForm.propTypes = {
    bannerData: PropTypes.object,
    isAdvanced: PropTypes.bool,
    updateMetadata: PropTypes.func.isRequired,
    cleanMetadata: PropTypes.func.isRequired,
};

BannerEditorForm.defaultProps = {
    bannerData: {},
    isAdvanced: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(BannerEditorForm);
