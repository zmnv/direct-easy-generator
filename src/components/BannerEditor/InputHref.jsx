import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

import { Label, Input } from '~/theme/components';
import { ADVEASY_TONOP_LINK } from '~/environment';

import Warning from './Warning';

const INPUT_STYLE = { maxWidth: 480 };

class InputHref extends PureComponent {
    setHref = event => {
        const href = event.target.value;
        this.props.onChange({ href });
    }

    render() {
        const { value } = this.props;

        return (
            <Fragment>
                <Label htmlFor="href">
                    <div className="mar-b-8">Ссылка на рекламное размещение</div>
                    <Input
                        type="text"
                        id="href"
                        name="href"
                        placeholder="Введите ссылку"
                        value={value}
                        onChange={this.setHref}
                        style={INPUT_STYLE}
                        required
                    />
                </Label>

                {value === ADVEASY_TONOP_LINK && (
                    <Warning className="mar-t-8 mar-b-8">
                        Не забудьте указать номер размещения.
                        Проверьте баннер, нажав на него.
                    </Warning>
                )}
            </Fragment>
        );
    }
}

InputHref.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
};

InputHref.defaultProps = {
    onChange: () => {},
};

export default InputHref;
