import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Label, Input, Button } from '~/theme/components';

class InputContent extends PureComponent {
    state = {
        isVisible: this.props.visible,
    }

    toggleVisible = () => {
        this.setState(({isVisible}) => ({isVisible: !isVisible}));
    }

    handleInputChange = event => {
        const { target: { value, name } } = event;
        this.props.onChange({
            [name]: value,
        });
    }

    render() {
        const { values: { title, description, image } } = this.props;
        const { isVisible } = this.state;

        if (!isVisible) {
            return (
                <div style={{position: 'absolute', right: '16px', top: '22px'}}>
                    <Button
                        transparentBackground
                        onClick={this.toggleVisible}
                    >
                        Редактировать контент
                    </Button>
                </div>
            );
        }

        return (
            <div className="mar-t-24 mar-b-8">
                <Label>Контент баннера</Label>
                <div style={{display: 'flex', marginTop: '16px'}}>
                    <form onSubmit={this.handleSubmit} autoComplete="off" style={{width: '100%'}}>
                        <div className="mar-b-16">
                            <Input
                                type="text"
                                name="title"
                                id="title"
                                placeholder="Заголовок"
                                value={title}
                                onChange={this.handleInputChange}
                                required
                            />
                        </div>

                        <div className="mar-b-16">
                            <Input
                                type="text"
                                name="description"
                                id="description"
                                placeholder="Описание"
                                value={description}
                                onChange={this.handleInputChange}
                            />
                        </div>

                        <div htmlFor="image">
                            <Input
                                type="text"
                                name="image"
                                id="image"
                                placeholder="Ссылка на изображение"
                                value={image}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </form>

                    <div className="mar-l-16" style={{flex: 0}}>
                        <Button color="mono" onClick={this.toggleVisible}>спрятать</Button>
                        <Button color="mono" transparentBackground onClick={this.props.onClean} className="mar-t-8">очистить</Button>
                    </div>
                </div>
            </div>
        );
    }
}

InputContent.propTypes = {
    visible: PropTypes.bool,
    values: PropTypes.object,
    onChange: PropTypes.func,
    onClean: PropTypes.func,
};

InputContent.defaultProps = {
    visible: false,
    values: {},
    onChange: () => {},
    onClean: () => {},
};

export default InputContent;
