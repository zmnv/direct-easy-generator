import React from 'react';

const HowToLink = () => (
    <a
        href="https://777999.au.ru/zmnv/direct/help"
        target="_blank"
        rel="noopener noreferrer"
    >
        Подробнее о загрузке баннера в «Адвизи»
    </a>
);

export default HowToLink;
