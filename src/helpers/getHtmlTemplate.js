import { Metadata } from './getMetaByUrl';

const LINES = 2;

const titleAdaptive = {
    web: {
        fontSize: 21,
        lineHeight: 26,
        fontWeight: 500,
    },
    mobile: {
        fontSize: 17,
        lineHeight: 22,
        fontWeight: 700,
    },
};

const descriptionAdaptive = {
    web: {
        fontSize: 15,
        lineHeight: 22,
    },
    mobile: {
        fontSize: 13,
        lineHeight: 18,
    },
};

const template = ({ title, description, image, href, isBlured, isContain }) => (/* html */`\
<!DOCTYPE html><html lang="ru">
<head>\
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">\
<meta name="generator" content="Au.ru Direct Banner Designer" />\
<meta name="copyright" content="Аукцион Au.ru" />\
<meta name="created" content="${new Date()}" />\
<style type="text/css">\
    /*! normalize.css v8.0.0 | MIT License | github.com/necolas/normalize.css */

    html {
        line-height: 1.15;
        -webkit-text-size-adjust: 100%
    }

    h1 {
        font-size: 2em;
        margin: .67em 0
    }

    hr {
        -webkit-box-sizing: content-box;
        -moz-box-sizing: content-box;
        box-sizing: content-box;
        height: 0;
        overflow: visible
    }

    pre {
        font-family: monospace, monospace;
        font-size: 1em
    }

    a {
        background-color: transparent
    }

    .ez-card__content-container {
        border: 1px solid rgba(0, 0, 0, .16);
        border-left: 0;
        border-radius: 4px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0
    }

    abbr[title] {
        border-bottom: none;
        text-decoration: underline;
        -webkit-text-decoration: underline dotted;
        -moz-text-decoration: underline dotted;
        text-decoration: underline dotted
    }

    b,
    strong {
        font-weight: bolder
    }

    code,
    kbd,
    samp {
        font-family: monospace, monospace;
        font-size: 1em
    }

    small {
        font-size: 80%
    }

    sub,
    sup {
        font-size: 75%;
        line-height: 0;
        position: relative;
        vertical-align: baseline
    }

    sub {
        bottom: -.25em
    }

    sup {
        top: -.5em
    }

    img {
        border-style: none
    }

    button,
    input,
    optgroup,
    select,
    textarea {
        font-family: inherit;
        font-size: 100%;
        line-height: 1.15;
        margin: 0
    }

    button,
    input {
        overflow: visible
    }

    button,
    select {
        text-transform: none
    }

    [type=button],
    [type=reset],
    [type=submit],
    button {
        -webkit-appearance: button
    }

    [type=button]::-moz-focus-inner,
    [type=reset]::-moz-focus-inner,
    [type=submit]::-moz-focus-inner,
    button::-moz-focus-inner {
        border-style: none;
        padding: 0
    }

    [type=button]:-moz-focusring,
    [type=reset]:-moz-focusring,
    [type=submit]:-moz-focusring,
    button:-moz-focusring {
        outline: 1px dotted ButtonText
    }

    fieldset {
        padding: .35em .75em .625em
    }

    legend {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        color: inherit;
        display: table;
        max-width: 100%;
        padding: 0;
        white-space: normal
    }

    progress {
        vertical-align: baseline
    }

    textarea {
        overflow: auto
    }

    [type=checkbox],
    [type=radio] {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 0
    }

    [type=number]::-webkit-inner-spin-button,
    [type=number]::-webkit-outer-spin-button {
        height: auto
    }

    [type=search] {
        -webkit-appearance: textfield;
        outline-offset: -2px
    }

    [type=search]::-webkit-search-decoration {
        -webkit-appearance: none
    }

    ::-webkit-file-upload-button {
        -webkit-appearance: button;
        font: inherit
    }

    details {
        display: block
    }

    summary {
        display: list-item
    }

    [hidden],
    template {
        display: none
    }

    * {
        -webkit-font-smoothing: antialiased;
        -webkit-overflow-scrolling: touch;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        -webkit-text-size-adjust: none;
        -webkit-touch-callout: none;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box
    }

    body {
        font-family: Arial, sans-serif;
        font-size: 16px;
        font-weight: 400;
        margin: 0;
        padding: 0
    }

    a {
        text-decoration: none;
        color: #0063e6
    }

    a:focus,
    a:hover {
        color: #e9413b
    }

    .container {
        max-width: 900px;
        margin: 64px auto 0;
        padding: 0 16px
    }

    .flex-justify {
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -webkit-flex-direction: row;
        -moz-box-orient: horizontal;
        -moz-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -moz-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-content: flex-start;
        -ms-flex-line-pack: start;
        align-content: flex-start;
        -webkit-box-align: start;
        -webkit-align-items: flex-start;
        -moz-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start
    }

    .flex-justify--center {
        -webkit-box-align: center;
        -webkit-align-items: center;
        -moz-box-align: center;
        -ms-flex-align: center;
        align-items: center
    }

    .flex {
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex
    }

    .flex-center {
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -moz-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center
    }

    .flex-center,
    .flex-vertical-center {
        -webkit-box-align: center;
        -webkit-align-items: center;
        -moz-box-align: center;
        -ms-flex-align: center;
        align-items: center
    }

    .flex-no-shrink {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto
    }

    .flex-shrink-element {
        -webkit-box-flex: 0;
        -webkit-flex: 0 auto;
        -moz-box-flex: 0;
        -ms-flex: 0 auto;
        flex: 0 auto
    }

    .flex-shrink-expand {
        -webkit-box-flex: 1;
        -webkit-flex: 1;
        -moz-box-flex: 1;
        -ms-flex: 1;
        flex: 1
    }

    .animate-rainbow {
        -webkit-animation: animate-rainbow 20s steps(130) infinite
    }

    .animate-rainbow-once {
        -webkit-animation: animate-rainbow .5s steps(16) 1;
        -webkit-animation-delay: .1s;
        animation-delay: .1s
    }

    @-webkit-keyframes animate-rainbow {
        0% {
            -webkit-filter: hue-rotate(10deg)
        }
        to {
            -webkit-filter: hue-rotate(1turn)
        }
    }

    @-webkit-keyframes au-design-rainbow {
        0% {
            -webkit-filter: hue-rotate(10deg);
            filter: hue-rotate(10deg)
        }
        to {
            -webkit-filter: hue-rotate(1turn);
            filter: hue-rotate(1turn)
        }
    }

    @keyframes au-design-rainbow {
        0% {
            -webkit-filter: hue-rotate(10deg);
            filter: hue-rotate(10deg)
        }
        to {
            -webkit-filter: hue-rotate(1turn);
            filter: hue-rotate(1turn)
        }
    }

    #root {
        height: 170px;
    }

    .ez-card {
        width: 100%;
        font-size: 15px;
        line-height: 20px;
        border-radius: 4px;
        text-align: left;
        background: #fff;
    }

    .ez-card,
    .ez-card__image-container {
        height: 100%;
        overflow: hidden;
        position: relative;
    }

    .ez-card__image-container {
        width: 170px
    }

    @media (min-width:425px) {
        .ez-card__image-container {
            width: 226px
        }
    }

    .ez-card__image-container--blured {
        position: absolute;
        z-index: 0;
        width: 200%;
        height: 200%;
        top: -50%;
        left: -50%;
        filter: blur(8px);
        -webkit-filter: blur(13px)
    }

    .ez-card__image,
    .ez-card__image-container--blured {
        background-repeat: no-repeat;
        background-size: cover;
        background-position: 50%
    }

    .ez-card__image {
        display: block;
        width: 100%;
        height: 100%;
        z-index: 1;
        position: relative;
        box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.16);
        border-radius: 4px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }

    .ez-card__image__overlay {
        position: absolute;
        background: rgba(0, 0, 0, .3);
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 2
    }

    .ez-card__image__overlay,
    .ez-card__image__overlay-button {
        opacity: 0;
        -webkit-transition: all .3s ease-in-out;
        -o-transition: all ease-in-out .3s;
        transition: all .3s ease-in-out
    }

    .ez-card__image__overlay-button {
        margin-top: 16px;
        display: block;
        width: 64px;
        height: 64px;
        border: 0;
        border-radius: 999px;
        background-position: 50%;
        background-repeat: no-repeat;
        background-size: 54px;
        background-color: transparent;
        background-image: url(data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjMyIiB3aWR0aD0iMzIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNMzIgMTYuMDA5YzAtLjI2Ny0uMTEtLjUyMi0uMjkzLS43MTRsLTkuODk5LTkuOTk5YS45OTMuOTkzIDAgMCAwLTEuNDE0IDAgMS4wMTYgMS4wMTYgMCAwIDAgMCAxLjQyOGw4LjE5MyA4LjI3NUgxYy0uNTUyIDAtMSAuNDUyLTEgMS4wMXMuNDQ4IDEuMDEgMSAxLjAxaDI3LjU4NmwtOC4xOTIgOC4yNzVhMS4wMTcgMS4wMTcgMCAwIDAgMCAxLjQyOC45OTQuOTk0IDAgMCAwIDEuNDE0IDBsOS44OTktOS45OTljLjE4Ny0uMTg5LjI5LS40NDkuMjkzLS43MTR6IiBmaWxsPSIjZmZmIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L3N2Zz4=)
    }

    .ez-card__image-container:hover .ez-card__image__overlay,
    .ez-card__image__overlay:hover .ez-card__image__overlay-button {
        opacity: 1;
        margin-top: 0
    }

    .ez-card__title {
        -webkit-transition: all 90ms ease-in-out;
        -o-transition: all ease-in-out 90ms;
        transition: all 90ms ease-in-out;
        display: block;
        color: #007fe4;
        overflow: hidden;
        margin-top: 10px;
        word-break: break-word;

        font-weight: ${titleAdaptive.mobile.fontWeight};
        font-size: ${titleAdaptive.mobile.fontSize}px;
        line-height: ${titleAdaptive.mobile.lineHeight}px;
        max-height: ${titleAdaptive.mobile.lineHeight * LINES}px;
    }

    @media (min-width:425px) {
        .ez-card__title {
            font-weight: ${titleAdaptive.web.fontWeight};
            font-size: ${titleAdaptive.web.fontSize}px;
            line-height: ${titleAdaptive.web.lineHeight}px;
            max-height: ${titleAdaptive.web.lineHeight * LINES}px;
        }
    }

    .ez-card__title:focus,
    .ez-card__title:hover {
        color: #ff5e00
    }

    .ez-card__description {
        -webkit-transition: all 90ms ease-in-out;
        -o-transition: all ease-in-out 90ms;
        transition: all 90ms ease-in-out;
        display: block;
        color: rgba(0, 0, 0, .7);
        overflow: hidden;
        margin-top: 6px;
        word-break: break-word;

        font-size: ${descriptionAdaptive.mobile.fontSize}px;
        line-height: ${descriptionAdaptive.mobile.lineHeight}px;
        max-height: ${descriptionAdaptive.mobile.lineHeight * LINES}px;
    }

    @media (min-width:425px) {
        .ez-card__description {
            font-size: ${descriptionAdaptive.web.fontSize}px;
            line-height: ${descriptionAdaptive.web.lineHeight}px;
            max-height: ${descriptionAdaptive.web.lineHeight * LINES}px;
        }
    }

    .ez-card__description:focus,
    .ez-card__description:hover {
        color: #ff5e00
    }

    .ez-card-padding {
        padding-left: 16px;
        padding-right: 16px
    }
</style>\
</head>\
<body>
    <div id="root">
        <div class="ez-card flex ez-card--out-view">
            <div class="ez-card__image-container flex-no-shrink">
                <a
                    href="${href}"
                    rel="nofollow"
                    target="_blank"
                    class="ez-card__image__overlay flex flex-center"
                >
                    <span class="ez-card__image__overlay-button">&nbsp;</span>
                </a>
                <a
                    href="${href}"
                    rel="nofollow"
                    target="_blank"
                    class="ez-card__image"
                    style="background-image:url(${image}); background-size: ${isContain ? 'contain' : 'cover'}"
                >
                    &nbsp;
                </a>
                ${isBlured ? `
                    <div class="ez-card__image-container--blured" style="background-image:url('${image}')"></div>
                ` : ''}
            </div>
            <div class="ez-card__content-container flex-shrink-expand">
                <a
                    href="${href}"
                    rel="nofollow"
                    target="_blank"
                    class="ez-card__title ez-card-padding"
                >
                    ${title}
                </a>
                <a
                    href="${href}"
                    rel="nofollow"
                    target="_blank"
                    class="ez-card__description ez-card-padding"
                >
                    ${description}
                </a></div>
        </div>
    </div>
</body></html>
`);

export const getHtmlTemplate = props => {
    const metadataProps = Metadata.create(props);

    const withoutSpaces = template(metadataProps).replace(/^\s+|\s+$/gm, ' ');
    return withoutSpaces.replace(/(\r\n|\n|\r)/gm, '');
};
