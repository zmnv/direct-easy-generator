const urlMetadata = require('url-metadata');

export const decodeHTML = (html) => {
    const txt = document.createElement('textarea');
    txt.innerHTML = html;
    return txt.value;
};

export class Metadata {
    constructor(raw) {
        this.title = raw.title || raw['og:title'] || 'Пустой заголовок';
        this.description = raw.description || raw['og:description'] || '';
        this.image = raw.image || raw['og:image'] || 'https://media2.24aul.ru/imgs/5cdb818019cf4e357cdcc2fe/';
        this.href = raw.href;
        this.isContain = raw.isContain;
        this.isBlured = raw.isBlured;
    }

    static create(raw) {
        return raw ? new Metadata(raw) : {};
    }
}

export async function getMetaByUrl(url, withModel) {
    const options = {
        encode(value) {
            return decodeHTML(value);
        },
    };

    const fromUrl = /au\.ru/.test(new URL(url))
        ? url
        : `https://cors-anywhere.herokuapp.com/${url}`;

    return urlMetadata(fromUrl, options).then(
        metadata => withModel ? Metadata.create(metadata) : metadata,
        error => Promise.reject(error),
    );
}
