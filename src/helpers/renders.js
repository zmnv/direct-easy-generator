import { renderToStaticMarkup } from 'react-dom/server';
import CleanCSS from 'clean-css';

import { initialCss } from './initialCss';

export function getDocumentStyles() {
    const documentStyles = [...document.getElementsByTagName('style')];
    const styles = [];

    documentStyles.forEach(elem => {
        if (elem.innerText && /sc-component/.test(elem.innerText)) {
            const style = new CleanCSS().minify(elem.innerText).styles;
            styles.push(`<style>${style}</style>`);
        }
    });

    return styles;
}

export function renderComponentAsHtmlString(Component) {
    const html = renderToStaticMarkup(Component);
    const styles = getDocumentStyles().join('');

    return `\
<head>\
<meta charset="utf-8" />\
<meta name="generator" content="Au.ru Direct Banner Designer" />\
<meta name="copyright" content="Аукцион Au.ru" />\
<meta name="created" content="${new Date()}" />\
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, minimum-scale=1" />\
</head>\
${initialCss}${styles}${html}`;
}
