/* eslint-disable jsx-a11y/no-autofocus */
import React from 'react';

export const AutofocusDisabler = () => {
    return (
        <input type="text" autoFocus style={{display: 'none'}} />
    );
};
