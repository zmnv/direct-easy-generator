import styled from 'styled-components';

import { colors, units } from '~/theme/vars';

const Input = styled.input`
    display: block;
    font-size: 16px;
    padding: ${units.u8}px ${units.u12}px;
    width: 100%;
    border: 1px solid;
    border-color: ${({highlight}) => highlight ? colors.interactive : colors.border};
    outline: none;
    
    transition: 0.2s all ease-in-out;

    &:focus {
        border-color: ${colors.interactive};
    }
`;

export default Input;
