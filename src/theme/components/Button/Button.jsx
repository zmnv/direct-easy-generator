import PropTypes from 'prop-types';
import styled from 'styled-components';

import { colors, units } from '~/theme/vars';

const variants = transparentBackground => ({
    default: {
        'background-color': !transparentBackground ? '#03a9f425' : 'transparent',
        color: colors.link,
    },
    mono: {
        'background-color': !transparentBackground ? '#efefef' : 'transparent',
        color: colors.text,
    },
});

const variantsHover = {
    default: {
        'background-color': '#03a9f450',
    },
    mono: {
        'background-color': '#cccccc',
    },
};

const Button = styled.button`
    font-size: 16px;
    font-weight: ${({bold}) => bold ? 600 : 400};
    border: 0;
    padding: ${units.u8}px ${units.u12}px;
    border-radius: ${units.u4 + 1}px;

    transition: 0.2s all ease-in-out;
    outline: none;
    cursor: pointer;

    ${({color, transparentBackground}) => variants(transparentBackground)[color] || variants.default}

    &:hover, &:focus {
        ${({color}) => variantsHover[color] || variantsHover.default}
    }

    &:active {
        opacity: 0.5;
    }
`;

Button.propTypes = {
    bold: PropTypes.bool,
    color: PropTypes.oneOf(['default', 'mono']),
};

Button.defaultProps = {
    bold: false,
    color: 'default',
};

export default Button;
