import styled from 'styled-components';

import { colors, units } from '~/theme/vars';

const Label = styled.label`
    display: block;
    font-size: 13px;
    color: ${colors.textGray};
    margin-bottom: ${units.u8}px;
    position: relative;
`;

export default Label;
