import React from 'react';
import PropTypes from 'prop-types';

const DoneSpan = ({visible}) => visible
    ? (
        <span className="mar-l-8" role="img" aria-label="done">
            👍
        </span>
    )
    : null;

DoneSpan.propTypes = {
    visible: PropTypes.bool.isRequired,
};

export default DoneSpan;
