import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';
import CheckboxModule from '@material/react-checkbox';

// import { as styles } from '~/theme';

const CheckboxLabel = styled.label`
    display: flex;
    font-size: 16px;
    position: relative;
    cursor: pointer;
`;

const Checkbox = props => {
    const { id, label, ...otherProps } = props;

    return (
        // eslint-disable-next-line jsx-a11y/label-has-for
        <CheckboxLabel htmlFor={id}>
            <CheckboxModule
                nativeControlId={id}
                {...otherProps}
            />
            <span style={{paddingTop: '12px'}}>
                {label}
            </span>
        </CheckboxLabel>
    );
};

Checkbox.propTypes = {
    id: PropTypes.string,
    label: PropTypes.string,
};

Checkbox.defaultProps = {
    id: 'simple',
    label: 'Чекбокс',
};

export default Checkbox;
