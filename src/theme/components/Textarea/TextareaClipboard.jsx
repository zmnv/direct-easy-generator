import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { CopyToClipboard } from 'react-copy-to-clipboard';

import { Button } from '~/theme/components/Button';
import { DoneSpan } from '~/theme/components/DoneSpan';

import Textarea from './Textarea';

class TextareaClipboard extends PureComponent {
    state = {
        isCopied: false,
    }

    handleCopied = () => {
        this.setState({isCopied: true}, () => {
            setTimeout(() => {
                this.setState({isCopied: false});
            }, 1000);
        });
    }

    render() {
        const { value, actionButtonLabel } = this.props;
        const { isCopied } = this.state;

        return (
            <div>
                <Textarea style={{color: 'gray'}} value={value} readOnly />
                <div className="mar-t-8">
                    <CopyToClipboard text={value} onCopy={this.handleCopied}>
                        <Button bold>
                            {actionButtonLabel}
                        </Button>
                    </CopyToClipboard>

                    <DoneSpan visible={isCopied} />
                </div>
            </div>
        );
    }
}

TextareaClipboard.propTypes = {
    value: PropTypes.string,
    actionButtonLabel: PropTypes.string,
};

TextareaClipboard.defaultProps = {
    value: '',
    actionButtonLabel: 'скопировать',
};

export default TextareaClipboard;
