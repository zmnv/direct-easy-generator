import styled from 'styled-components';
import { colors } from '~/theme/vars';

const Textarea = styled.textarea`
    width: 100%;
    min-height: 68px;
    font-size: 13px;
    border-color: ${({highlight}) => highlight ? colors.interactive : colors.border};
    outline: none;
    resize: vertical;
    
    transition: 0.2s border-color ease-in-out;

    &:focus {
        border-color: ${colors.interactive};
    }
`;

export default Textarea;
