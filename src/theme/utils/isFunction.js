export const isFunction = func => typeof func === 'function' ? func : null;
