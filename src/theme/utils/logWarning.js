/**
 * Вывести YellowBox с предупреждением  
 * (только в DEV-режиме)
 */
export function logWarning(...args) {
    const [message, ...other] = args;

    if (__DEV__) {
        // eslint-disable-next-line no-console
        console.warn(message, ...other);
    }
}
