export const colors = {
    background: '#fff',
    text: '#222',
    textGray: '#777',
    link: '#0d77cc',
    linkHover: '#de3502',
    interactive: '#03a9f4',
    border: '#e5e5e5',
};
