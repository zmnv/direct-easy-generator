/** 
 * **Набор значений для 8dp (4dp) сетки**
 * 
 * https://material.io/design/layout/understanding-layout.html#usage
 */
export const units = {
    u0: 0,
    u2: 2, // +
    u4: 4,
    u8: 8,
    u12: 12,
    u16: 16,
    u20: 20,
    u24: 24,
    u28: 28,
    u32: 32,
    u36: 36,
    u40: 40,
    u44: 44,
    u48: 48,
    u52: 52,
    u56: 56,
    u60: 60,
    u64: 64,
    u68: 68,
    u72: 72,
    u76: 76,
    u80: 80,
    u96: 96, // +
    u128: 128, // +
    u192: 192, // +
    u256: 256, // +
};
