import React, { PureComponent } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { Provider } from 'react-redux';

import { ScrollToTop } from '~/theme/components';

import { Dashboard } from '~/apps/dashboard';

import { configureStore } from '~/store';

import '~/theme/styles.scss';

import '~/theme/tools/bootstrap-grid.scss';
import '~/theme/tools/layout.scss';
import '~/theme/tools/zmnv-offsets.scss';

import '~/theme/tools/checkbox.css';
import '~/theme/tools/snackbar.css';
import '~/theme/tools/button.css';

class App extends PureComponent {
    render() {
        return (
            <Provider store={configureStore()}>
                <Router>
                    <ScrollToTop>
                        <Dashboard />
                    </ScrollToTop>
                </Router>
            </Provider>
        );
    }
}

export default App;
