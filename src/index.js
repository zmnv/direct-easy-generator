import React from 'react';
import ReactDOM from 'react-dom';
// import { renderToString } from 'react-dom/server';
// import { render as renderSnapshot } from 'react-snapshot';

import App from './App';

// eslint-disable-next-line react/no-render-return-value
const render = Component => ReactDOM.render(
    <Component />,
    document.getElementById('root'),
);

// const render = Component => renderSnapshot(
//     <Component />,
//     document.getElementById('root'),
// );

render(App);

if (module.hot) {
    module.hot.accept('./App', () => {
        const NextApp = require('./App').default;
        render(NextApp);
    });
}
