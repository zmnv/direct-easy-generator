import { combineReducers } from 'redux';

import { dashboardReducer } from '~/apps/dashboard/store';

export default combineReducers({
    dashboard: dashboardReducer,
});
