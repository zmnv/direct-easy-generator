import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './rootReducer';

export function configureStore() {
    const middlewaresList = [thunk];

    const appliedMiddlewares = applyMiddleware(...middlewaresList);
    const middlewares = process.env.NODE_ENV !== 'production'
        ? composeWithDevTools(appliedMiddlewares)
        : appliedMiddlewares;

    const store = createStore(
        rootReducer,
        middlewares,
    );

    if (module.hot) {
        module.hot.accept(() => {
            // eslint-disable-next-line global-require
            const nextRootReducer = require('./rootReducer').default;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}

export const store = configureStore();
