import { createSelector } from 'reselect';

const getMetadataFromDashboard = state => (state.dashboard && state.dashboard.data) || {};

export const getBannerMetadata = createSelector(
    getMetadataFromDashboard,
    (bannerMetadata) => bannerMetadata,
);
