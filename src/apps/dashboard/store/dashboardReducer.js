import { dashboardActionsTypes as types } from './dashboardActionsTypes';
import { ADVEASY_TONOP_LINK } from '~/environment';

const initialState = {
    data: {
        title: '',
        description: '',
        image: '',
        href: ADVEASY_TONOP_LINK,
        isContain: false,
        isBlured: false,
    },
    isLoading: false,
    error: null,
};

export function dashboardReducer(state = initialState, action) {
    switch (action.type) {
    case types.requestMetadata:
        return {
            ...state,
            isLoading: true,
        };

    case types.requestFailedMetadata: {
        const { payload } = action;

        return {
            ...state,
            isLoading: false,
            error: payload,
        };
    }

    case types.updateMetadata: {
        const { payload = {} } = action;

        // if (!payload.title) {
        //     return state;
        // }

        const data = {
            ...state.data,
            ...payload,
        };

        return {
            ...state,
            data,
            isLoading: false,
        };
    }

    case types.cleanMetadata:
        return {
            ...state,
            data: {
                ...state.data,
                title: initialState.data.title,
                description: initialState.data.description,
                image: initialState.data.image,
            },
        };

    default:
        return state;
    }
}
