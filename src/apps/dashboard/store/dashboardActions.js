import { dashboardActionsTypes as types} from './dashboardActionsTypes';

export const dashboardRequestMetadata = () => ({
    type: types.requestMetadata,
});

export const dashboardRequestFailedMetadata = payload => ({
    type: types.requestFailedMetadata,
    payload,
});

export const dashboardUpdateMetadata = payload => ({
    type: types.updateMetadata,
    payload,
});

export const dashboardCleanMetadata = () => ({
    type: types.cleanMetadata,
});
