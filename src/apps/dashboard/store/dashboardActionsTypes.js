export const dashboardActionsTypes = {
    requestMetadata: '[Dashboard] Request Metadata',
    requestFailedMetadata: '[Dashboard] Request Metadata Failed',
    updateMetadata: '[Dashboard] Update Metadata',
    cleanMetadata: '[Dashboard] Clean Metadata',
};
