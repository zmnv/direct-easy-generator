import { dashboardRequestMetadata, dashboardUpdateMetadata, dashboardRequestFailedMetadata} from './dashboardActions';

import { getMetaByUrl } from '~/helpers/getMetaByUrl';

/**
 * Получение списка уведомлений
 *
 * @param {number} count - количество уведомлений
 */
export function fetchMetadata(url, callback = () => {}) {
    return (dispatch) => {
        dispatch(dashboardRequestMetadata());

        getMetaByUrl(url, false)
            .then((metadata = {}) => {
                if (!metadata.title) {
                    const emptyError = 'Не удалось получить текст заголовока по ссылке';
                    console.log(emptyError);

                    dispatch(dashboardRequestFailedMetadata(emptyError));
                    return;
                }

                dispatch(dashboardUpdateMetadata(metadata));
                callback();
            })
            .catch(error => {
                const fetchError = 'Ошибка получения метаданных по ссылке';
                console.log(fetchError, error);

                dispatch(dashboardRequestFailedMetadata(fetchError));
            });
    };
}
