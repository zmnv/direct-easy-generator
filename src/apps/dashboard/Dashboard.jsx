import React, { PureComponent } from 'react';

import Modal from 'react-responsive-modal';

import { HowToLink } from '~/components/HowToLink';
import { MetadataFetcher } from '~/components/MetaDataFetcher';
import { Button } from '~/theme/components/Button';

import { BannerEditorResult, BannerEditorForm } from '~/components/BannerEditor';

class Dashboard extends PureComponent {
    state = {
        resultVisible: false,
        advancedModal: false,
    }

    onOpenModal = event => {
        const advancedModal = !!(event && event.target && event.target.name);

        this.setState({ resultVisible: true, advancedModal });
    };

    onCloseModal = () => {
        this.setState({ resultVisible: false });
    };

    render() {
        const { resultVisible, advancedModal } = this.state;

        return (
            <div>
                <div className="mar-b-48">
                    <div style={{width: '100%', maxWidth: '500px', margin: '0 auto', marginTop: '64px'}}>
                        <h1 className="mar-b-64 text-center">Ау-Директ</h1>
                        <MetadataFetcher onMetadataReady={this.onOpenModal} />
                    </div>
                </div>

                <div className="col-12 mar-t-64 text-center">
                    <Button
                        name="advancedModal"
                        transparentBackground
                        onClick={this.onOpenModal}
                        className="mar-r-16"
                    >
                        Создать баннер вручную
                    </Button>
                    <HowToLink />
                </div>

                <Modal open={resultVisible} onClose={this.onCloseModal} center>
                    <div style={{minWidth: '760px'}}>
                        <div className="mar-b-16">
                            <BannerEditorForm isAdvanced={advancedModal} />
                        </div>
                        <BannerEditorResult />
                    </div>
                </Modal>
            </div>
        );
    }
}

export default Dashboard;
