/* eslint-disable no-param-reassign */
const path = require('path');
const webpack = require('webpack');

module.exports = function override(config) {
    config.resolve = {
        ...config.resolve,
        alias: { '~': path.resolve(__dirname, './src') },
    };

    // config.module = {
    //     ...config.module,
    //     rules: [
    //         ...config.module.rules,
    //         {
    //             test: /\.(png|jp(e*)g|svg)$/,
    //             use: [{
    //                 loader: 'url-loader',
    //                 options: {
    //                     limit: 8000, // Convert images < 8kb to base64 strings
    //                     // name: 'images/[hash]-[name].[ext]',
    //                 },
    //             }],
    //         },
    //     ],
    // };

    config.plugins.push(
        new webpack.DefinePlugin({
            SC_DISABLE_SPEEDY: true,
        }),
    );

    return config;
};
