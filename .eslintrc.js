const path = require('path');

const ext = [
    '.js',
    '.jsx',
];

module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: 'airbnb',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    __DEV__: true,
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'import',
  ],
  rules: {
    'global-require': 'off',
    'no-irregular-whitespace': ["error", { "skipComments": true }],
    'no-trailing-spaces': ["error", { "ignoreComments": true }],
    'import/prefer-default-export': 0,
    'import/no-extraneous-dependencies': ["error", {
        "packageDir": './',
    }],
    'react/jsx-filename-extension': 'off',
    'react/destructuring-assignment': 'off',
    'react/forbid-prop-types': 'off',
    'react/jsx-indent-props': [2, 4],
    'react/jsx-indent': [2, 4],
    'react/jsx-one-expression-per-line': 'off',
    'react/prefer-stateless-function': [1, { "ignorePureComponents": true }],
    'class-methods-use-this': 'off',
    'no-confusing-arrow': 'off',
    'object-curly-spacing': 'off',
    'object-curly-newline': 'off',
    'arrow-parens': 'off',
    'indent': ['error', 4],
  },
  settings: {
    "import/resolver": {
      node: {
        extensions: ext,
        paths: [path.resolve(__dirname, 'src')],
      },
      alias: {
        extensions: ext,
        map: [
            ["~", './src'],
        ]
      },
    }
  },
};
